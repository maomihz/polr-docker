FROM alpine:3.15
MAINTAINER kolaente - mowie.cc

# Install NGINX and PHP 7
RUN apk update && \
    apk --no-cache add bash tzdata curl ca-certificates s6 ssmtp mysql-client \
    nginx nginx-mod-http-headers-more

RUN apk --no-cache add \
    php7 php7-phar php7-curl php7-fpm php7-json php7-zlib php7-gd \
    php7-xml php7-dom php7-ctype php7-opcache php7-zip php7-iconv \
    php7-pdo php7-pdo_mysql php7-mysqli php7-mbstring php7-session \
    php7-mcrypt php7-openssl php7-sockets php7-posix php7-mbstring php7-gettext php7-tokenizer php7-xmlwriter

RUN ln -sf /usr/bin/php7 /usr/bin/php && \
    rm -f /etc/php7/php-fpm.d/www.conf && \
    touch /etc/php7/php-fpm.d/env.conf && \
    rm -rf /var/www

# Copy Config
COPY conf/services.d /etc/services.d
COPY conf/nginx/nginx.conf /etc/nginx/nginx.conf
COPY conf/php/php-fpm.conf /etc/php7/php-fpm.conf
COPY conf/php/conf.d/php.ini /etc/php7/conf.d/zphp.ini

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy Polr
COPY --chown=nginx:nginx polr-master /var/www
WORKDIR /var/www

# Install Polr dependencies
USER nginx
RUN php /usr/local/bin/composer install --no-dev -o

EXPOSE 8080

USER root
ENTRYPOINT ["/bin/s6-svscan", "/etc/services.d"]

CMD []
